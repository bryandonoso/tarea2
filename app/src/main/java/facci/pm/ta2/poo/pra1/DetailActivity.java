package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    //Se declara las variables que se va a utilizar
    TextView precio, descripcion, nombre;
    ImageView IMAGEN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        // Se utilizan las variables creadas anteriormente y captamos los valores precio, nombre, descripcion y imagen

        String object = getIntent().getStringExtra("object_id");
        precio = (TextView) findViewById(R.id.precio);
        nombre = (TextView)findViewById(R.id.nombre);
        descripcion = (TextView)findViewById(R.id.description);
        IMAGEN = (ImageView)findViewById(R.id.Imangen);

        //Se accede a las propiedades del objecto de tipo String.
        DataQuery query = DataQuery.get("item");
        String parametro = getIntent().getExtras().getString("objeto1");

        //Recibe el parametro añadido en el Layout activity_detail.xml
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    //Procedemos a recibir a las propiedades del objecto de tipo String:
                    // name, price, description e imagen mediante un identificador.

                    String Precio = (String) object.get("price")+("\u0024");
                    String Descripcion = (String) object.get("description");
                    String Nombre = (String) object.get("name");
                    Bitmap ImagenBitmap = (Bitmap) object.get("image");

                    //Se guardan los datos de los parametros
                    precio.setText(Precio);
                    descripcion.setText(Descripcion);
                    nombre.setText(Nombre);
                    IMAGEN.setImageBitmap(ImagenBitmap);
                    // FIN - CODE6
                }
            }
        });

        // FIN - CODE6

    }

}
